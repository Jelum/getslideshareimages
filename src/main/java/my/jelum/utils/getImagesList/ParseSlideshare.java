package my.jelum.utils.getImagesList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class ParseSlideshare {
    private static final Logger log = Logger.getLogger(ParseSlideshare.class);

    private static boolean downloadFiles(List<String> filesList, String folderToSave) {
	ReadableByteChannel rbc = null;
	FileOutputStream fos = null;

	File dir = new File(folderToSave);
	if (!dir.mkdir()) {
	    if (!dir.exists() || !dir.canWrite()) {
		log.fatal("downloadFiles - Cannot create folder");
		return false;
	    }
	}

	try {
	    String fileName = null;
	    URL website = null;
	    for (String url : filesList) {
		website = new URL(url);
		String[] fileNameParts = url.split("/");
		fileName = fileNameParts[fileNameParts.length - 1];
		rbc = Channels.newChannel(website.openStream());
		fos = new FileOutputStream(String.format("%s/%s", folderToSave, fileName));
		fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		log.info("Downloaded: " + fileName);

		fos.close();
		fos = null;
		rbc.close();
		rbc = null;
	    }
	    return true;
	} catch (IOException e) {
	    log.fatal("downloadFiles : " + e.getMessage());
	    return false;
	} finally {
	    if (fos != null) {
		try {
		    fos.close();
		} catch (IOException e) {
		}
	    }
	    if (rbc != null) {
		try {
		    rbc.close();
		} catch (IOException e) {
		}
	    }
	}
    }

    private static boolean downloadHtmlPage(String urlString, String outFileName) {
	URL url;
	try {
	    url = new URL(urlString);
	} catch (MalformedURLException e) {
	    log.fatal("downloadHtmlPage - " + e.getMessage());
	    return false;
	}
	try (InputStream is = url.openStream(); OutputStream output = new FileOutputStream(outFileName)) {
	    byte[] buf = new byte[1024];
	    int bytesRead;
	    while ((bytesRead = is.read(buf)) > 0) {
		output.write(buf, 0, bytesRead);
	    }
	    return true;
	} catch (IOException e) {
	    log.fatal("downloadHtmlPage - " + e.getMessage());
	    return false;
	}
    }

    private static String editURL(String url) {
	String[] tmp = url.split("/");
	String folderToSave = tmp[tmp.length - 1];
	log.debug(String.format("editURL - Folder is %s", folderToSave));

	StringBuffer sb = new StringBuffer("http://www.slideshare.net/fullscreen/");
	sb.append(url.substring("http://www.slideshare.net/".length()));
	url = sb.toString();
	log.debug("editURL - URL changed");

	return folderToSave;
    }

    private static List<String> getFilesList(String inFileName) {
	List<String> list = new ArrayList<>();
	try (FileReader fileReader = new FileReader(inFileName);
		BufferedReader bufferedReader = new BufferedReader(fileReader)) {
	    int i = 0;
	    String line;
	    while ((line = bufferedReader.readLine()) != null) {
		if (line.contains("data-full=")) {
		    int begin = line.indexOf("data-full");
		    int end = line.indexOf("alt=");
		    log.debug(String.format("Begin - %d, end - %d", begin, end));
		    line = line.substring(begin, end);
		    line = line.split("\"")[1];
		    line = line.split("\\?")[0];
		    list.add(line);
		    log.debug("Line add");
		    i++;
		}
	    }
	    log.info(String.format("Added %d lines", i));
	} catch (IOException e) {
	    log.fatal("getFilesList - " + e.getMessage());
	}
	File file = new File(inFileName);
	if (!file.delete()) {
	    file.deleteOnExit();
	}
	return list;
    }

    public static boolean getSlideshareImages(String url) {
	if (!StringUtils.isEmpty(url)) {
	    log.info("Start");
	    String folderToSave = ParseSlideshare.editURL(url);
	    if (!ParseSlideshare.downloadHtmlPage(url, folderToSave + ".html")) {
		log.error("Couldn't download html page");
		return false;
	    }
	    List<String> list = ParseSlideshare.getFilesList(folderToSave + ".html");
	    if (!ParseSlideshare.downloadFiles(list, folderToSave)) {
		return false;
	    }
	    log.info("End");
	    return true;
	} else {
	    log.error("URL is empty");
	    return false;
	}
    }

    public static void main(String[] args) {
	ParseSlideshare.getSlideshareImages(args[0]);
    }

}