package my.jelum.utils.getImagesList;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class ParseSlideshareGUI extends JFrame {
    private static final long serialVersionUID = -2574060273999092972L;
    private static final Logger log = Logger.getLogger(ParseSlideshareGUI.class);

    private JTextField textField = null;
    private JPanel panel = null;

    public ParseSlideshareGUI() {
	setTitle("ParseSlideshare");
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setLocation(200, 200);

	createPanel();
	add(panel);

	pack();
	setVisible(true);
    }

    private void createPanel() {
	panel = new JPanel();
	textField = new JTextField("Put URL");
	textField.addMouseListener(new MouseAdapter() {
	    @Override
	    public void mouseClicked(MouseEvent e) {
		super.mouseClicked(e);
		textField.setText("");
	    }
	});
	textField.setColumns(50);

	JButton button = new JButton();
	button.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent e) {
		String url = textField.getText();
		if (StringUtils.isEmpty(url)) {
		    JOptionPane.showMessageDialog(panel, "URL is empty", "ERROR", JOptionPane.ERROR_MESSAGE);
		} else {
		    try {
			boolean response = ParseSlideshare.getSlideshareImages(url);

			if (response) {
			    JOptionPane.showMessageDialog(panel, "Done", "End", JOptionPane.INFORMATION_MESSAGE);
			} else {
			    JOptionPane.showMessageDialog(panel, "Fail", "End", JOptionPane.ERROR_MESSAGE);
			}
		    } catch (Exception ex) {
			JOptionPane.showMessageDialog(panel, "An expected error", "ERROR", JOptionPane.ERROR_MESSAGE);
		    }
		}
	    }
	});
	button.setText("Download");

	panel.add(textField);
	panel.add(button);
    }

    public static void main(String[] args) {
	EventQueue.invokeLater(new Runnable() {
	    public void run() {
		try {
		    new ParseSlideshareGUI();
		} catch (Exception exception) {
		    log.fatal(exception.getMessage());
		}
	    }
	});
    }

}
